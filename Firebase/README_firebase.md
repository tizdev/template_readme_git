# Back end - [Name of the app]
[random_firebase_functions_app](https://bitbucket.org/tizdev/template_readme_git/src/master/)

---

- Firebase Cloud Functions
- Node.js 10
- Status `DEV` / `TEST` / `PROD` 

---

## Table of contents

* [About the project](#markdown-header-about-the-project)
    * [Contributors](#markdown-header-contributors)
    * [Description](#markdown-header-description)
    * [Plugins](#markdown-header-plugins)
    * [Environments](#markdown-header-environments)
* [Getting started](#markdown-header-getting-started)
    * [Installation](#markdown-header-installation)
    * [Credentials](#markdown-header-credentials)
* [Test and deploy](#markdown-header-test-and-deploy)
    * [Deploy method](#markdown-header-deploy-method) 
* [Frequently asked questions](#markdown-header-frequently-asked-questions)
  

---

## About the project
### Contributors

 * Developers : xxxx XXXX, xxxx XXXX
 * Designer : xxxx XXXX
 * Project manager : xxxx XXXX
 * Marketing : xxxx XXXX
 * Commercial : xxxx XXXX
 

### Description

Template for the README of Git projects. This need to be use in new project created. This section need to describe briefly the project, and how it works.


### Plugins

|        Tool       | Version |
| ----------------- | ------- |
| firebase-admin    | 8.6.0   |
| firebase-function | 3.3.0   |
 
 
### Environments

 * Stage + Prod : [Firebase](https://console.firebase.google.com/u/0/)

Need to auth with apiculteurstiz Google account.

Front : [STAGE](https://example.com/) / [PROD](https://example.com/)

---

## Getting started

### Installation

```sh
$ npm install
$ npm install -g firebase-tools
$ firebase login
```

### Credentials

You need to create the following keys :
 
 * someservice.api_key_test
 * someservice.secret_key_test

 * someservice.api_key_prod
 * someservice.secret_key_prod
 

Available in Lastpass

See the `Frequently asked questions` section to know how to add a new key.


---

## Test and deploy

### Deploy method

* For all functions 

```sh
$ firebase deploy --only functions
```
or

```sh
$ firebase deploy
```


* For one specific function

```sh
$ firebase deploy --only functions:XXXXXXXX
```

---

## Frequently asked questions

##### How to store a new key

```sh
$ firebase functions:config:set someservice.key="API KEY" 
```

Exemple :

```sh
$ firebase functions:config:set stripe.apiKey="XXXXXXXXXX"
```

##### How to check the stored keys

```sh
$ firebase functions:config:get
```