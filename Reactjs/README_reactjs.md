# Name of the ReactJS app  
[random_reactjs_site](https://bitbucket.org/tizdev/template_readme_git/src/master/)

---

- ReactJS [version]
- Status `DEV` / `TEST` / `PROD` 

---

## Table of contents

* [About the project](#markdown-header-about-the-project)
    * [Contributors](#markdown-header-contributors)
    * [Description](#markdown-header-description)
    * [Plugins](#markdown-header-plugins)
    * [Environments](#markdown-header-environments)
* [Getting started](#markdown-header-getting-started)
    * [Prerequisites](#markdown-header-prerequisites)
    * [Installation](#markdown-header-installation)
    * [Credentials](#markdown-header-credentials)
* [Production](#markdown-header-test-and-deploy)
    * [Create the environments](#markdown-header-create-the-environments) 
* [Test and deploy](#markdown-header-test-and-deploy)
    * [Run](#markdown-header-run)
    * [Test](#markdown-header-test)
    * [Build](#markdown-header-build)
    * [Deploy](#markdown-header-deploy)
    * [Eject](#markdown-header-eject)
* [ReactJS tools](#markdown-header-tools)
    * [Code splitting](#markdown-header-code-splitting)
    * [Analyzing the bundle size](#markdown-header-analyzing-the-bundle-size)
    * [Making a progressive web app](#markdown-header-making-a-progressive-web-app)
    * [Advanced configuration](#markdown-header-advanced-configuration)
* [Frequently asked questions](#markdown-header-frequently-asked-questions)

---

## About the project

### Contributors

 * Developers : xxxx XXXX, xxxx XXXX
 * Designer : xxxx XXXX
 * Project manager : xxxx XXXX
 * Marketing : xxxx XXXX
 * Commercial : xxxx XXXX
 

### Description

![Alt text](https://i.ibb.co/YDKNjBm/screenshot.png)

Template for the README of Git projects. This need to be use in new project created. This section need to describe briefly the project, and how it works.

### Plugins
 * List of plugins used
 
 
### Environments

 * Preprod :  https://example.com/
 * Prod : https://example.com/

Back-Office : https://example.com/

---

## Getting started
### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

* npm
```sh
$ npm install npm@latest -g
```

### Installation

1. Install dependencies
```sh
$ npm install 
```

* ReactJS
```sh
$ npm install --global create-react-app
```


### Credentials
Available in Lastpass.


## Production

### Create the environments

1. Create the VHOST and the folders structure on servers like :
- `prod/<domain>/<subdomain>/public/web`
- `preprod/<domain>/<subdomain>/public/web`

2. Add the domain name to [Gandhi](https://id.gandi.net/fr/) like `<exemple>.tiz.fr`

3. Create SSH local key to access to servers (stage + prod)

4. Clone the git repository into the folders (stage + prod)

5. Tiz need to add the SSH key to bitbucket


---

## Test and deploy

### Run

```sh
$ npm run start
```

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.


### Test

```sh
$ npm run test
```

Launches the test runner in the interactive watch mode.

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


### Build

```sh
$ npm run build
```

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

The app wil be deploy in preprod environment.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### Deploy

Use the [ReactJS Pipeline](https://bitbucket.org/tizdev/pipeline-reactjs/src/master/) to deploy your apps. 

### Eject

```sh
$ npm run eject
```

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

---


## ReactJS tools

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
To learn React, check out the [React documentation](https://reactjs.org/).

### Code splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the bundle size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a progressive web app

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration


--- 

## Frequently asked questions

##### Question 1
 * Answer 1

##### Question 2
 * Answer 2