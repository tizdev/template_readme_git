# Name of the Xamarin app 
[random_xamarin_app](https://bitbucket.org/tizdev/template_readme_git/src/master/)

---

- Xamarin
- Status `DEV` / `TEST` / `PROD` 

---

## Table of contents

* [About the project](#markdown-header-about-the-project)
    * [Contributors](#markdown-header-contributors)
    * [Description](#markdown-header-description)
    * [Plugins](#markdown-header-plugins)
    * [Environments](#markdown-header-environments)
* [Getting started](#markdown-header-getting-started)
    * [Prerequisites](#markdown-header-prerequisites)
    * [Installation](#markdown-header-installation)
    * [Credentials](#markdown-header-credentials)
* [Test and deploy](#markdown-header-test-and-deploy)
    * [Run](#markdown-header-run) 
    * [Deploy](#markdown-header-deploy) 
* [Frequently asked questions](#markdown-header-frequently-asked-questions)
  

---

## About the project
### Contributors

 * Developers : xxxx XXXX, xxxx XXXX
 * Designer : xxxx XXXX
 * Project manager : xxxx XXXX
 * Marketing : xxxx XXXX
 * Commercial : xxxx XXXX
 

### Description

![Alt text](https://i.ibb.co/YDKNjBm/screenshot.png)

Template for the README of Git projects. This need to be use in new project created. This section need to describe briefly the project, and how it works.


### Plugins
 * List of plugins used
 
 
### Environments

App :

 * [Android - Google Play](https://example.com/)
 * [iOS - App Store](https://example.com/)

Back-Office : 

[back_office_name](https://example.com/)

 * PREPROD : [https://example.com/](https://example.com/) 
 * PROD : [https://example.com/](https://example.com/) 

---

## Getting started
### Prerequisites

 * [Visual Studio](https://visualstudio.microsoft.com/fr/) (not Visual Studio Code) 
 * [Android Studio](https://developer.android.com/studio)
 * [XCode](https://apps.apple.com/fr/app/xcode/id497799835?mt=12)


### Installation

 * Launch the project on Visual studio

...

### Credentials
Available in Lastpass, and in the Google Drive


---

## Test and deploy

### Run

Android

 * Go to the top left bar, and select `KPM.Android > debug > xxxxxxxxx`
 * Then click on the play icon

iOS

 * Go to the top left bar, and select `KPM.iOS > debug > xxxxxxxxx`
 * Then click on the play icon


### Deploy

Android

 * Go to `KPM.Android > Properties > AndroidManifest.xml` and update the `Version number` and `Version name` values.
 * Go to the top center bar and select `Build > Clean all` and `Build > Build all`
 * If all fine, click on `Build > Archive for publishing` (after selecting KPM.Android folder).
 * Wait, then select your archive, and click on `Save on disk (ad hoc)`
 * Use the keykpm key to sign the app (available on Google Drive, and credentials in Lastpass)
 * Add the signed apk to Google Play (with the apiculteurstiz account)


iOS

 * Go to `KPM.iOS > info.plist` and update the `Bundle version` and `Bundle version number (string)` values.
 * On the top left bar, select `KPM.iOS > release > Generic device`
 * Go to the top center bar and select `Build > Clean all` and `Build > Build all`
 * If all fine, click on `Build > Archive for publishing` (after selecting KPM.iOS folder).
 * Wait, then select your archive, and click on `App Store : Save to Disk and publish to App Store`
 * Click `Upload`, then use the `Profile KPM` provisonning profile
 * Use the apiculteurstiz App store credentials to login & upload the file to App store Connect
 * Then go to [App Store Connect](https://appstoreconnect.apple.com/), and create a new release with the new archive upload (it can take time to appear to App Store Connect)

...


---

## Frequently asked questions

##### Question 1
 * Answer 1

##### Question 2
 * Answer 2